package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/hue-exporter/pkg/hue"
)

type HuePower struct {
	Level *prometheus.Desc
}

func NewHuePower() *HuePower {
	return &HuePower{
		Level: prometheus.NewDesc(power, "The current battery state in percent", powerLabels, nil),
	}
}

func (s *HuePower) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *HuePower) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for Device Power")
	huePowers := hue.GetPowers()
	for _, hp := range huePowers.Data {
		meta := GetMetaData(hp.Owner.Rid)
		c <- prometheus.MustNewConstMetric(s.Level, prometheus.GaugeValue, float64(hp.PowerState.BatteryLevel), hp.ID, meta.DeviceID, meta.DeviceName, meta.ModelID, meta.ProductName, meta.ProductArchetype, meta.ManufacturerName, meta.RoomID, meta.RoomName, meta.RoomArchetype)
	}
}
