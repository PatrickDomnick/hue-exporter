package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/hue-exporter/pkg/hue"
)

type HueMotion struct {
	Motion *prometheus.Desc
}

func NewHueMotion() *HueMotion {
	return &HueMotion{
		Motion: prometheus.NewDesc(motion, "1 if motion is detected", motionLabels, nil),
	}
}

func (s *HueMotion) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *HueMotion) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for Motion")
	hueMotions := hue.GetMotions()
	for _, hm := range hueMotions.Data {
		is_motion := float64(0)
		if hm.Motion.Motion {
			is_motion = float64(1)
		}
		meta := GetMetaData(hm.Owner.Rid)
		c <- prometheus.MustNewConstMetric(s.Motion, prometheus.GaugeValue, is_motion, hm.ID, meta.DeviceID, meta.DeviceName, meta.ModelID, meta.ProductName, meta.ProductArchetype, meta.ManufacturerName, meta.RoomID, meta.RoomName, meta.RoomArchetype)
	}
}
