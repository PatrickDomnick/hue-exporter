package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/hue-exporter/pkg/hue"
)

type HueLevel struct {
	Level *prometheus.Desc
}

func NewHueLevel() *HueLevel {
	return &HueLevel{
		Level: prometheus.NewDesc(level, "Light level in 10000*log10(lux) + 1", levelLabels, nil),
	}
}

func (s *HueLevel) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *HueLevel) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for Light Levels")
	hueLevels := hue.GetLevels()
	for _, hl := range hueLevels.Data {
		meta := GetMetaData(hl.Owner.Rid)
		c <- prometheus.MustNewConstMetric(s.Level, prometheus.GaugeValue, float64(hl.Light.LightLevel), hl.ID, meta.DeviceID, meta.DeviceName, meta.ModelID, meta.ProductName, meta.ProductArchetype, meta.ManufacturerName, meta.RoomID, meta.RoomName, meta.RoomArchetype)
	}
}
