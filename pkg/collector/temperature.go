package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/hue-exporter/pkg/hue"
)

type HueTemperature struct {
	Temperature *prometheus.Desc
}

func NewHueTemperature() *HueTemperature {
	return &HueTemperature{
		Temperature: prometheus.NewDesc(temperature, "Temperature in 1.00 degrees Celsius", temperatureLabels, nil),
	}
}

func (s *HueTemperature) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *HueTemperature) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for Device Temperature")
	hueTemperatures := hue.GetTemperatures()
	for _, ht := range hueTemperatures.Data {
		meta := GetMetaData(ht.Owner.Rid)
		c <- prometheus.MustNewConstMetric(s.Temperature, prometheus.GaugeValue, ht.Temperature.Temperature, ht.ID, meta.DeviceID, meta.DeviceName, meta.ModelID, meta.ProductName, meta.ProductArchetype, meta.ManufacturerName, meta.RoomID, meta.RoomName, meta.RoomArchetype)
	}
}
