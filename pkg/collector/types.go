package collector

const onState = "hue_light_on"
const brightness = "hue_light_brightness"
const colorX = "hue_light_color_x"
const colorY = "hue_light_color_y"
const power = "hue_power_level"
const level = "hue_level"
const motion = "hue_motion"
const temperature = "hue_temperature"

var defaultLabels = []string{"id", "deviceId", "deviceName", "modelId", "productName", "productArchetype", "manufacturerName", "roomId", "roomName", "roomArchetype"}
var lightLabels = append(defaultLabels, "zoneIds", "zoneNames", "zoneArchetypes", "name")
var powerLabels = defaultLabels
var levelLabels = defaultLabels
var motionLabels = defaultLabels
var temperatureLabels = defaultLabels
