package collector

import (
	"strings"

	"gitlab.com/PatrickDomnick/hue-exporter/pkg/hue"
)

var Devices hue.HueDevice
var Rooms hue.HueRoom
var Zones hue.HueZone

type HueMetaData struct {
	DeviceID         string
	DeviceName       string
	ModelID          string
	ProductName      string
	ProductArchetype string
	ManufacturerName string
	RoomID           string
	RoomName         string
	RoomArchetype    string
}

func GetMetaData(deviceId string) *HueMetaData {
	// Get Device MetaData
	meta := &HueMetaData{}
	for _, hd := range Devices.Data {
		if hd.ID == deviceId {
			meta.DeviceID = hd.ID
			meta.DeviceName = hd.Metadata.Name
			meta.ModelID = hd.ProductData.ModelID
			meta.ProductName = hd.ProductData.ProductName
			meta.ProductArchetype = hd.ProductData.ProductArchetype
			meta.ManufacturerName = hd.ProductData.ManufacturerName
		}
	}
	// Get Room Name
	for _, hr := range Rooms.Data {
		for _, hrc := range hr.Children {
			if hrc.Rid == meta.DeviceID {
				meta.RoomID = hr.ID
				meta.RoomName = hr.Metadata.Name
				meta.RoomArchetype = hr.Metadata.Archetype
			}
		}
	}
	return meta
}

type HueLightZone struct {
	ZoneIDs        string
	ZoneNames      string
	ZoneArchetypes string
}

func GetMetaZone(lightId string) *HueLightZone {
	zone := &HueLightZone{}
	// Get Zones
	zid := []string{}
	zname := []string{}
	ztype := []string{}
	for _, hz := range Zones.Data {
		for _, hzc := range hz.Children {
			if hzc.Rid == lightId {
				zid = append(zid, hz.ID)
				zname = append(zname, hz.Metadata.Name)
				ztype = append(ztype, hz.Metadata.Archetype)
			}
		}
	}
	// Get Device MetaData
	zone.ZoneIDs = strings.Join(zid, ",")
	zone.ZoneNames = strings.Join(zname, ",")
	zone.ZoneArchetypes = strings.Join(ztype, ",")
	return zone
}
