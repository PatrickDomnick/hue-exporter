package collector

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/PatrickDomnick/hue-exporter/pkg/hue"
)

type HueLight struct {
	LightOn    *prometheus.Desc
	Brightness *prometheus.Desc
	ColorX     *prometheus.Desc
	ColorY     *prometheus.Desc
}

func NewHueLight() *HueLight {
	return &HueLight{
		LightOn:    prometheus.NewDesc(onState, "On/Off state of the light", lightLabels, nil),
		Brightness: prometheus.NewDesc(brightness, "Brightness percentage", lightLabels, nil),
		ColorX:     prometheus.NewDesc(colorX, "X position in CIE gamut position", lightLabels, nil),
		ColorY:     prometheus.NewDesc(colorY, "Y position in CIE gamut position", lightLabels, nil),
	}
}

func (s *HueLight) Describe(c chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(s, c)
}

func (s *HueLight) Collect(c chan<- prometheus.Metric) {
	log.Info("Collecting Metrics for Lights")
	// Level
	hueLights := hue.GetLights()
	for _, hl := range hueLights.Data {
		meta := GetMetaData(hl.Owner.Rid)
		zoneData := GetMetaZone(hl.ID)
		// Operation State
		state := float64(0)
		if hl.On.On {
			state = float64(1)
		}
		c <- prometheus.MustNewConstMetric(s.LightOn, prometheus.GaugeValue, state, hl.ID, meta.DeviceID, meta.DeviceName, meta.ModelID, meta.ProductName, meta.ProductArchetype, meta.ManufacturerName, meta.RoomID, meta.RoomName, meta.RoomArchetype, zoneData.ZoneIDs, zoneData.ZoneNames, zoneData.ZoneArchetypes, hl.Metadata.Name)
		c <- prometheus.MustNewConstMetric(s.Brightness, prometheus.GaugeValue, hl.Dimming.Brightness, hl.ID, meta.DeviceID, meta.DeviceName, meta.ModelID, meta.ProductName, meta.ProductArchetype, meta.ManufacturerName, meta.RoomID, meta.RoomName, meta.RoomArchetype, zoneData.ZoneIDs, zoneData.ZoneNames, zoneData.ZoneArchetypes, hl.Metadata.Name)
		c <- prometheus.MustNewConstMetric(s.ColorX, prometheus.GaugeValue, hl.Color.Xy.X, hl.ID, meta.DeviceID, meta.DeviceName, meta.ModelID, meta.ProductName, meta.ProductArchetype, meta.ManufacturerName, meta.RoomID, meta.RoomName, meta.RoomArchetype, zoneData.ZoneIDs, zoneData.ZoneNames, zoneData.ZoneArchetypes, hl.Metadata.Name)
		c <- prometheus.MustNewConstMetric(s.ColorY, prometheus.GaugeValue, hl.Color.Xy.Y, hl.ID, meta.DeviceID, meta.DeviceName, meta.ModelID, meta.ProductName, meta.ProductArchetype, meta.ManufacturerName, meta.RoomID, meta.RoomName, meta.RoomArchetype, zoneData.ZoneIDs, zoneData.ZoneNames, zoneData.ZoneArchetypes, hl.Metadata.Name)
	}
}
