package hue

const deviceUrl = "device"
const roomUrl = "room"
const zoneUrl = "zone"
const lightUrl = "light"
const powerUrl = "device_power"
const levelUrl = "light_level"
const motionUrl = "motion"
const temperatureUrl = "temperature"
