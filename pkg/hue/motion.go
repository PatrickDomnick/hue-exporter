package hue

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type hueMotion struct {
	Data   []struct {
		Enabled bool   `json:"enabled"`
		ID      string `json:"id"`
		IDV1    string `json:"id_v1"`
		Motion  struct {
			Motion      bool `json:"motion"`
			MotionValid bool `json:"motion_valid"`
		} `json:"motion"`
		Owner struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"owner"`
		Type string `json:"type"`
	} `json:"data"`
}

func GetMotions() hueMotion {
	// Devices Endpoint
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", motionUrl))

	motionApiResponse := hueMotion{}

	// Execute the Bosch API Call
	err := getJSON(motionUrl, &motionApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return motionApiResponse
}
