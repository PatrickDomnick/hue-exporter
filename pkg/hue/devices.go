package hue

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type HueDevice struct {
	Data   []struct {
		ID       string `json:"id"`
		IDV1     string `json:"id_v1"`
		Metadata struct {
			Archetype string `json:"archetype"`
			Name      string `json:"name"`
		} `json:"metadata"`
		ProductData struct {
			Certified        bool   `json:"certified"`
			ManufacturerName string `json:"manufacturer_name"`
			ModelID          string `json:"model_id"`
			ProductArchetype string `json:"product_archetype"`
			ProductName      string `json:"product_name"`
			SoftwareVersion  string `json:"software_version"`
		} `json:"product_data"`
		Services []struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"services"`
		Type string `json:"type"`
	} `json:"data"`
}

func GetDevices() HueDevice {
	// Devices Endpoint
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", deviceUrl))

	deviceApiResponse := HueDevice{}

	// Execute the Bosch API Call
	err := getJSON(deviceUrl, &deviceApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return deviceApiResponse
}
