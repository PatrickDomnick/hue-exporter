package hue

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type hueTemperature struct {
	Data []struct {
		Enabled bool   `json:"enabled"`
		ID      string `json:"id"`
		IDV1    string `json:"id_v1"`
		Owner   struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"owner"`
		Temperature struct {
			Temperature      float64 `json:"temperature"`
			TemperatureValid bool    `json:"temperature_valid"`
		} `json:"temperature"`
		Type string `json:"type"`
	} `json:"data"`
}

func GetTemperatures() hueTemperature {
	// Devices Endpoint
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", temperatureUrl))

	temperatureApiResponse := hueTemperature{}

	// Execute the Bosch API Call
	err := getJSON(temperatureUrl, &temperatureApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return temperatureApiResponse
}
