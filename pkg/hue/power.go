package hue

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type huePower struct {
	Data []struct {
		ID    string `json:"id"`
		IDV1  string `json:"id_v1"`
		Owner struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"owner"`
		PowerState struct {
			BatteryLevel int    `json:"battery_level"`
			BatteryState string `json:"battery_state"`
		} `json:"power_state"`
		Type string `json:"type"`
	} `json:"data"`
}

func GetPowers() huePower {
	// Devices Endpoint
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", powerUrl))

	powerApiResponse := huePower{}

	// Execute the Bosch API Call
	err := getJSON(powerUrl, &powerApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return powerApiResponse
}
