package hue

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type hueLevel struct {
	Data   []struct {
		Enabled bool   `json:"enabled"`
		ID      string `json:"id"`
		IDV1    string `json:"id_v1"`
		Light   struct {
			LightLevel      int  `json:"light_level"`
			LightLevelValid bool `json:"light_level_valid"`
		} `json:"light"`
		Owner struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"owner"`
		Type string `json:"type"`
	} `json:"data"`
}

func GetLevels() hueLevel {
	// Devices Endpoint
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", levelUrl))

	levelApiResponse := hueLevel{}

	// Execute the Bosch API Call
	err := getJSON(levelUrl, &levelApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return levelApiResponse
}
