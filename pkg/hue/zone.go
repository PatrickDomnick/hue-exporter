package hue

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type HueZone struct {
	Data []struct {
		Children []struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"children"`
		GroupedServices []struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"grouped_services"`
		ID       string `json:"id"`
		IDV1     string `json:"id_v1"`
		Metadata struct {
			Archetype string `json:"archetype"`
			Name      string `json:"name"`
		} `json:"metadata"`
		Services []struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"services"`
		Type string `json:"type"`
	} `json:"data"`
}

func GetZones() HueZone {
	// Devices Endpoint
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", zoneUrl))

	zoneApiResponse := HueZone{}

	// Execute the Bosch API Call
	err := getJSON(zoneUrl, &zoneApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return zoneApiResponse
}
