package hue

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

type hueLight struct {
	Data []struct {
		Alert struct {
			ActionValues []string `json:"action_values"`
		} `json:"alert"`
		Color struct {
			Gamut struct {
				Blue struct {
					X float64 `json:"x"`
					Y float64 `json:"y"`
				} `json:"blue"`
				Green struct {
					X float64 `json:"x"`
					Y float64 `json:"y"`
				} `json:"green"`
				Red struct {
					X float64 `json:"x"`
					Y float64 `json:"y"`
				} `json:"red"`
			} `json:"gamut"`
			GamutType string `json:"gamut_type"`
			Xy        struct {
				X float64 `json:"x"`
				Y float64 `json:"y"`
			} `json:"xy"`
		} `json:"color,omitempty"`
		ColorTemperature struct {
			Mirek       interface{} `json:"mirek"`
			MirekSchema struct {
				MirekMaximum int `json:"mirek_maximum"`
				MirekMinimum int `json:"mirek_minimum"`
			} `json:"mirek_schema"`
			MirekValid bool `json:"mirek_valid"`
		} `json:"color_temperature,omitempty"`
		Dimming struct {
			Brightness  float64 `json:"brightness"`
			MinDimLevel float64 `json:"min_dim_level"`
		} `json:"dimming,omitempty"`
		Dynamics struct {
			Speed        float64  `json:"speed"`
			SpeedValid   bool     `json:"speed_valid"`
			Status       string   `json:"status"`
			StatusValues []string `json:"status_values"`
		} `json:"dynamics"`
		ID       string `json:"id"`
		IDV1     string `json:"id_v1"`
		Metadata struct {
			Archetype string `json:"archetype"`
			Name      string `json:"name"`
		} `json:"metadata"`
		Mode string `json:"mode"`
		On   struct {
			On bool `json:"on"`
		} `json:"on"`
		Owner struct {
			Rid   string `json:"rid"`
			Rtype string `json:"rtype"`
		} `json:"owner"`
		Type string `json:"type"`
	} `json:"data"`
}

func GetLights() hueLight {
	// Devices Endpoint
	log.Info(fmt.Sprintf("Checking the following URL: '%s'", lightUrl))

	lightApiResponse := hueLight{}

	// Execute the Bosch API Call
	err := getJSON(lightUrl, &lightApiResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	return lightApiResponse
}
