FROM scratch
LABEL maintainer="patrickfdomnick@gmail.com"

# Copy from Dist
ARG PLATFORM
COPY dist/${PLATFORM}/hue-exporter /

# Expose Port and run Exporter
EXPOSE 8080/tcp
ENTRYPOINT ["/hue-exporter"]
